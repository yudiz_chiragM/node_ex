var express = require('express'),
    user = require('./services/user'),
    bodyParser = require('body-parser'),
    expressValidator = require('express-validator'),
    encrypt = require('bcrypt');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator())
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var route = express.Router();

route.post('/signup', user.signup);
route.post("/login", user.login);
route.post("/listusers", user.list);
route.post("/adduser", user.add);
route.post("/changestatus", user.changeStatus);
route.post("/deleteuser", user.deleteUser);


app.use('/', route);

app.listen(3001);