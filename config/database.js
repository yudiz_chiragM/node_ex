var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    port: 8889,
    database: 'test'
});
connection.connect(function(err) {
    if (err) {
        console.log('error in database connection', err);
    }
});

module.exports = connection;