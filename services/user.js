var db = require('../config/database'),
    expressValidator = require('express-validator'),
    encrypt = require('bcrypt');
exports.signup = function(req, res) {
    req.checkBody({
        name: {
            notEmpty: true
        },
        email: {
            notEmpty: true,
            isEmail: {
                errorMessage: "Invalid Email address"
            }
        },
        password: {
            notEmpty: true
        }
    });
    var errors = req.validationErrors();
    if (!errors) {
        var created_at = new Date();
        encrypt.hash(req.body.password, 5, function(err, encryptedPassword) {
            var data = "'" + req.body.name + "','" + req.body.email + "','" + encryptedPassword + "','" + created_at + "','" + created_at + "'";
            db.query("CALL signup(" + data + ")", function(error, resp, fields) {
                if (error) {
                    console.log('error while inserting data', err);
                    res.status(200).json({ status: 401, failed: "error while insert" });
                } else {
                    res.status(200).json({ status: 200, success: "Successfully registered" });
                }
            });
        });
    } else {
        res.status(412).json({ status: 412, message: "Fill all values properly", errors: errors });
    }
}

exports.login = function(req, res) {
    req.checkBody({
        email: {
            notEmpty: true,
            isEmail: {
                errorMessage: "Invalid Email address"
            }
        },
        password: {
            notEmpty: true
        }
    });
    var errors = req.validationErrors();
    if (!errors) {
        db.query('CALL login("' + req.body.email + '")', function(err, result, fields) {
            if (err) {
                res.status(401).json({ status: 401, err: "invalid username or password" });
            } else {
                if (result[0].length > 0) {
                    user = result[0][0];
                    encrypt.compare(req.body.password, user.password, function(err, isMatched) {
                        if (isMatched) {
                            res.status(200).json({ status: 200, user: user, message: "successfully logged in" });
                        } else {
                            console.log('not matched');
                        }
                    });
                } else {
                    res.status(401).json({ status: 401, err: "invalid username or password" });

                }
            }
        });
    } else {
        res.status(412).json({ status: 412, message: "Fill all values properly", errors: errors });
    }


}

exports.list = function(req, res) {
    req.checkBody({
        offset: {
            notEmpty: true
        },
        limit: {
            notEmpty: true
        }
    });
    var errors = req.validationErrors();
    db.query('CALL list_users("' + req.body.offset + '","' + req.body.limit + '")', function(err, result, fields) {
        if (err) {
            res.status(400).json({ status: 400, error: "Something went wrong" });
        } else {
            if (result[0].length > 0) {
                res.status(200).json({ status: 200, users: result[0][0] });
            } else {
                res.status(404).json({ status: 404, users: {}, message: "No User found" });
            }
        }
    });
}

exports.add = function(req, res) {
    req.checkBody({
        name: {
            notEmpty: true
        },
        email: {
            notEmpty: true,
            isEmail: {
                errorMessage: "Invalid Email address"
            }
        },
        password: {
            notEmpty: true
        }
    });
    var errors = req.validationErrors();
    if (!errors) {
        var created_at = new Date();
        encrypt.hash(req.body.password, 5, function(err, encryptedPassword) {
            var data = "'" + req.body.name + "','" + req.body.email + "','" + encryptedPassword + "','" + created_at + "','" + created_at + "'";
            db.query("CALL signup(" + data + ")", function(error, resp, fields) {
                if (error) {
                    console.log('error while inserting data', err);
                    res.status(200).json({ status: 401, failed: "error while insert" });
                } else {
                    res.status(200).json({ status: 200, success: "Successfully added" });
                }
            });
        });
    } else {
        res.status(412).json({ status: 412, message: "Fill all values properly", errors: errors });
    }
}

exports.changeStatus = function(req, res) {
    req.checkBody({
        status: {
            notEmpty: true
        }
    });
    errors = req.validationErrors();
    if (!errors) {
        db.query('CALL change_status("' + req.body.user_id + '","' + req.body.status + '")', function(err, result, fields) {
            if (err) {
                res.status(400).json({ status: 400, message: "something went wrong" });
            } else {
                if (result.affectedRows > 0) {
                    res.status(200).json({ status: 200, message: "Status updated successfully" });
                } else {
                    res.status(404).json({ status: 404, message: "User not found" });

                }
            }
        });
    } else {
        res.status(412).json({ status: 412, message: "Fill all values properly", errors: errors });

    }
}

exports.deleteUser = function(req, res) {
    req.checkBody({
        user_id: {
            notEmpty: true
        }
    });
    errors = req.validationErrors();
    if (!errors) {
        db.query('CALL delete_user("' + req.body.user_id + '")', function(err, result, fields) {
            if (err) {
                res.status(400).json({ status: 400, message: "something went wrong" });
            } else {
                if (result.affectedRows > 0) {
                    res.status(200).json({ status: 200, message: "user deleted successfully" });
                } else {
                    res.status(404).json({ status: 404, message: "User not found" });

                }
            }
        });
    }
}